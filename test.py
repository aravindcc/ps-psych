data = [
    0xab18, 0xdb14, 0x1b10, 0xab14, 0xbb1c, 0xa010, 0x2b10, 0x1b10, 0xa018, 0x1b14
]

for x in data:
    for y in data:
        if x < y and abs(int(x) - int(y)) <= 16:
            print(f"{hex(x)}: {hex(y)}: {int(x) - int(y)}")
